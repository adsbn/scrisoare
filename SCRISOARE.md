# Scrisoare deschisă despre GovITHub

Stimate domnule prim-ministru Daniel Cioloș, stimați membrii a consiliui GovITHub,

Ne bucurăm de inițiativa de care a făcut dovada Guvernul prin lansarea GovITHub și considerăm că este un mod excelent pentru noi, cei din sfera IT să contribuim la bunul public din țară.

Pentru o colaborare eficientă vă rugăm să clarificați punctul Dumneavoastră de vedere în legătură cu două întrebări:

## Sunt pozițiile de fellowship limitate la București?

Pe pagina ”Despre Program” [^1] scrie ”fellowship (poziție remunerată competitiv, full-time, la București)”.

Considerăm că expertiza IT românească este distribuită în toată țara și în toată lumea. În aceste condiții ar fi neprielnică limitarea pozițiilor la București. Majoritatea lucrăm cu clienți din toată lumea (din SUA până în Tokyo) și avem o colaborare eficientă cu ei. Nu ar fi nici o dificultate să colaborăm cu colegi din țară, de care ne separ doar câteva sute de kilometrii nu zeci de mii.

## Care este punctul de vedere a Guvernul despre dezvoltarea transparentă și sub licențe libere [^2] a programelor soft?

Considerăm că dezvoltarea în mod transparent [^3] și sub licențe libere duc la un produs final de o calitate mai înaltă.

Totodată considerăm ca soluțiile trebuie dezvoltate în spiritul programelor libere și publicate sub astfel de licențe din următoarele motive:

* Banii publici trebuie cheltuiți în scopuri publice: investiția într-un program soft liber pune la dispoziția publicului acel program (pentru modificări, îmbunătățiri sau testare și asigurarea calității)
* Flexibilitatea: statul poate contracta îmbunătățirea programelor soft fără restricții (nu mai depinde de dezvoltatorii inițiali)
* Investiția în dezvoltarea de tehnologii software în loc de simpla achiziție de licențe
* Soluțiile publicate sub o licență liberă pot fi refolosite și adaptate ușor de alte departamente
* Soluțiile publicate sub o licență liberă pot fi exemple pentru studenți să înțeleagă mai bine pașii necesari pentru crearea unui proiect de succes
* Există o multitudine de organizații la nivel național și internațional care ajută proiectele publicate sub licențe libere cu care am putea colabora [^4]
* Există deja un număr mare de organizații și companii care publică sau contribuie la proiecte aflate sub licențe libere [^5]
* Programele libere reprezintă vârful tehnologiei software în momentul de față

## În concluzie propunem:

* Desemnarea unor poziții din cadrul GovITHub persoanelor care pot colabora de la distanță (remote) folosind tehnologii precum: e-mail, mesagerie instant și conferențiere audio/video etc. pentru comunicare și coordonare.
* Toate informațiile legate de GovITHub și proiectele propuse și acceptate să fie disponibile în mod public și în timp real.
* Toate proiectele să fie dezvoltate în mod transparent iar dezvoltarea să se facă în mod public folosind platforme publice pentru publicarea codului în toate stagiile și toate ramurile de producție.
* Toate proiectele să fie disponibile sub o licență liberă pentru impactul pozitiv asupra societății (Bani publici pentru scopuri publice).

[^1]: [http://ithub.gov.ro/despre-noi/](http://ithub.gov.ro/despre-noi/)
[^2]: [https://www.gnu.org/philosophy/free-sw.ro.html](https://www.gnu.org/philosophy/free-sw.ro.html)
[^3]: * Planificarea și comunicarea să se facă în mod public, folosind tehnologii precum: e-mail, mesagerie instant, wiki-uri, forum-uri, etc.
 * Artefactele produse (codul sursă, imagini, etc) să fie disponibile în timp real publicului printr-un sistem de versionare (de exemplu Git, Mercurial, etc)
 * Să se accepte contribuții de la orice persoană interesată
[^4]: exemple: Free Software Foundation Europe, Software Freedom Law Center, Software in the Public Interest, Shuttleworth Foundation, GitLab, Mozilla Foundation, Google
[^5]: exemple: Google, Microsoft, Facebook, Redhat

Semnatarii,  
(accesați [SEMNATARII](SEMNATARII.md) pentru o listă completă)

